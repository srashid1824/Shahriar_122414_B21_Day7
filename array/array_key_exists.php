<?php
/*The array_key_exists() function checks an array for a specified key, and returns true if the key exists and false if the key does not exist.*/
$a=array("Volvo"=>"XC90","BMW"=>"X5");
if(array_key_exists("Volvo",$a)){
    echo "Key Up!";
}
else{
    echo "Key Not Found";
}
echo "<br>";
if(array_key_exists("Toyota",$a)){
    echo "Key Up!";
}
else{
    echo "Key Not Found";
}
?>