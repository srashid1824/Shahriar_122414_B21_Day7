<?php
/*The ltrim() function removes whitespace or other predefined characters from the left side of a string.
*
*/
$str = "\n\n\nHello World!";
echo $str;
echo "<br>";
echo "Without ltrim: " . $str;
echo "<br>";
echo "With ltrim: " . ltrim($str);
?>