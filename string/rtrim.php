<?php
/*The rtrim() function removes whitespace or other predefined characters from the right side of a string.*/
$str = "Hello World!";
echo $str . "<br>";
echo rtrim($str,"World!");
?>