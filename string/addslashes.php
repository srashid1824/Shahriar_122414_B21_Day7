<?php
/*
*The addslashes() function returns a string with backslashes in front of predefined characters.

*The predefined characters are:

*single quote (')
*double quote (")
*backslash (\)
*NULL
*/
$str = addslashes('What does \yolo\ mean?');
echo($str);
?>